function showDomain (D, titleString)
% showDomain  Draw a population domain.
%
%   showDomain (D, titleString);
%
% Draws a checkerboard plot of the domain D in the current figure
% window, with the given title. Assumes the values in the domain all
% lie between "-1" and "1".
  
  imagesc (D);
  axis square;
  title (titleString);
  colorbar;
  caxis ([-1 1]);
end

% eof
