function incentive = calculateIncentive(map)
    %finds ares that need incentive to stay
    incentiveAreas =  findIncentiveAreas(map);
    [m, n] = size(incentiveAreas);
    incentive = zeros(m, n);
    %go through each spot and calculate prob. that incentive is enough
    for i = 1:m
        for j = 1:n
            if (incentiveAreas(i ,j) && (randi([0,2] > 0)))
                %If the index in incentive is 1, they won't move
                incentive(i,j) = 1;
            end
        end
    end

end

function incentiveStatus = findIncentiveAreas(map)
    %this function will generate a mxn array
    %1's indicate the area needs government incentives
    %0's indicate that it does not
    [m, n] = size (map);
    incentiveStatus = zeros(m, n);
	%iterate through succesive squares
    for i = 1:m-1
        for j = 1:n-1
			%find the value of the color of the square of 4
			colorValue  = map(i, j) + map(i + 1, j) + map(i, j + 1) + map(i + 1,j + 1);
            if (abs(colorValue) < 3)
                %mark the spots as needing incentives
                incentiveStatus(i, j) = 1;
                incentiveStatus(i + 1, j) = 1;
                incentiveStatus(i, j + 1) = 1;
                incentiveStatus(i + 1, j + 1) = 1;
            end
        end
    end
end