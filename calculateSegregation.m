function seg = calculateSegregation(X)
	[m, n] = size (X);
	%check bounds
	if (m < 2 || n < 2)
		seg = -1;
		return;
	end

	seg = 0;
	%iterate through
    for i = 1: m - 1
        for j = 1: n - 1
			%find the value of the color of the square of 4
			colorValue  = X(i, j) + X(i + 1, j) + X(i, j + 1) + X(i + 1,j + 1);
			%add to the running tally of the segregation score
			seg = seg + abs(colorValue);
        end
    end
    
    %divide segregation score by the size of the grid
    
    seg = seg / (m*n);
end